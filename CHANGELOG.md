# ChangeLog

## Unreleased

**Added:**

**Changed:**

**Removed:**

**Fixed:**

## [4.3.1](https://gitlab.com/european-data-portal/mqa/shacl-validation/tags/4.3.1) (2019-05-06)
Initial release