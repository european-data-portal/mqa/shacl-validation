# shacl validation
Microservice for validating RDF graphs based on [SHACL](https://www.w3.org/TR/shacl/).

With special support for [DCAT-AP](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe) and [DCAT-AP SHACLS rules](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe/distribution/dcat-ap-12-shacl).

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Interface](#interface)
1. [Docker](#docker)
1. [Usage](#usage)
1. [License](#license)

## Build
Requirements:
 * Git
 * Maven
 * Java
 * (Docker)

```bash
$ git clone https://gitlab.com/european-data-portal/harvester/shacl-validation.git
$ cd shacl-validation
$ mvn package
```

## Run

```bash
$ java -jar target/shacl-validation-far.jar
```

## Interface

The documentation of the REST interface can be found when the root context is opened in a browser. The instance of the European Data Portal can be found here: https://www.europeandataportal.eu/shacl/

## Docker

Build docker image:
```bash
$ docker build -t shacl-validation .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 shacl-validation
```

## Usage

You can use [Postman](https://www.getpostman.com/) for easy testing your files with the service hosted by the European Data Portal. Postman is an application that provides an UI in order to support users with the creation of HTTP requests. In Postman you can import the [`edp_postman_validation_examples.json`](https://gitlab.com/european-data-portal/mqa/shacl-validation/snippets/1855458) file. This includes two example validation requests. One with an invalid dataset example and a second with a valid dataset example. If you want to use or own data, just replace example data with your file in the body of the validation request. Press `send` to submit the request.

The example request with invalid data should return a report that indicates the compliant issues.

```json
{
    "@graph": [
        {
            "@id": "_:b0",
            "@type": "sh:ValidationReport",
            "result": [
                "_:b1",
                "_:b2",
                "_:b3"
            ]
        },
        {
            "@id": "_:b1",
            "@type": "sh:ValidationResult",
            "focusNode": "https://europeandataportal.eu/set/data/530ef092454ae327f8491bed",
            "resultMessage": "Value does not have class http://www.w3.org/2004/02/skos/core#Concept",
            "resultPath": "dct:publisher",
            "resultSeverity": "sh:Violation",
            "sourceConstraintComponent": "sh:ClassConstraintComponent",
            "sourceShape": "_:b4",
            "value": "https://lorem.ipsum/organizations/5c0eefbb454ae320decd1ab6/"
        },
        {
            "@id": "_:b2",
            "@type": "sh:ValidationResult",
            "focusNode": "https://europeandataportal.eu/set/data/530ef092454ae327f8491bed",
            "resultMessage": "Value does not have shape http://data.europa.eu/r5r/mdrcv#CorporateBodyRestriction",
            "resultPath": "dct:publisher",
            "resultSeverity": "sh:Violation",
            "sourceConstraintComponent": "sh:NodeConstraintComponent",
            "sourceShape": "_:b4",
            "value": "https://lorem.ipsum/organizations/5c0eefbb454ae320decd1ab6/"
        },
        {
            "@id": "_:b3",
            "@type": "sh:ValidationResult",
            "focusNode": "https://europeandataportal.eu/set/data/530ef092454ae327f8491bed",
            "resultMessage": "Value does not have class http://xmlns.com/foaf/0.1/Agent",
            "resultPath": "dct:publisher",
            "resultSeverity": "sh:Violation",
            "sourceConstraintComponent": "sh:ClassConstraintComponent",
            "sourceShape": "_:b5",
            "value": "https://lorem.ipsum/organizations/5c0eefbb454ae320decd1ab6/"
        }
    ],
    "@context": {
        "result": {
            "@id": "http://www.w3.org/ns/shacl#result",
            "@type": "@id"
        },
        "value": {
            "@id": "http://www.w3.org/ns/shacl#value",
            "@type": "@id"
        },
        "resultPath": {
            "@id": "http://www.w3.org/ns/shacl#resultPath",
            "@type": "@id"
        },
        "resultMessage": {
            "@id": "http://www.w3.org/ns/shacl#resultMessage"
        },
        "focusNode": {
            "@id": "http://www.w3.org/ns/shacl#focusNode",
            "@type": "@id"
        },
        "sourceShape": {
            "@id": "http://www.w3.org/ns/shacl#sourceShape",
            "@type": "@id"
        },
        "sourceConstraintComponent": {
            "@id": "http://www.w3.org/ns/shacl#sourceConstraintComponent",
            "@type": "@id"
        },
        "resultSeverity": {
            "@id": "http://www.w3.org/ns/shacl#resultSeverity",
            "@type": "@id"
        },
        "@vocab": "http://www.semanticweb.org/owl/owlapi/turtle#",
        "schema": "http://schema.org/",
        "cc": "http://creativecommons.org/ns#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "xhv": "http://www.w3.org/1999/xhtml/vocab#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "skos": "http://www.w3.org/2004/02/skos/core#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "at": "http://publications.europa.eu/ontology/authority/",
        "dct": "http://purl.org/dc/terms/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "sh": "http://www.w3.org/ns/shacl#",
        "wdrs": "http://www.w3.org/2007/05/powder-s#",
        "dcterms": "http://purl.org/dc/terms/",
        "atold": "http://publications.europa.eu/resource/authority/",
        "dcat": "http://www.w3.org/ns/dcat#",
        "vann": "http://purl.org/vocab/vann/",
        "foaf": "http://xmlns.com/foaf/0.1/",
        "dc": "http://purl.org/dc/elements/1.1/"
    }
}
```

The example with the valid data should return a response like this.

```json
{
    "@id": "_:b0",
    "@type": "sh:ValidationReport",
    "@context": {
        "@vocab": "http://www.semanticweb.org/owl/owlapi/turtle#",
        "schema": "http://schema.org/",
        "cc": "http://creativecommons.org/ns#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "xhv": "http://www.w3.org/1999/xhtml/vocab#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "skos": "http://www.w3.org/2004/02/skos/core#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "at": "http://publications.europa.eu/ontology/authority/",
        "dct": "http://purl.org/dc/terms/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "sh": "http://www.w3.org/ns/shacl#",
        "wdrs": "http://www.w3.org/2007/05/powder-s#",
        "dcterms": "http://purl.org/dc/terms/",
        "atold": "http://publications.europa.eu/resource/authority/",
        "dcat": "http://www.w3.org/ns/dcat#",
        "vann": "http://purl.org/vocab/vann/",
        "foaf": "http://xmlns.com/foaf/0.1/",
        "dc": "http://purl.org/dc/elements/1.1/"
    }
}
```

## License

[Apache License, Version 2.0](LICENSE.md)
