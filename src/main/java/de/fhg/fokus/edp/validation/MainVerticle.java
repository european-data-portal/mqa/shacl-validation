package de.fhg.fokus.edp.validation;

import de.fhg.fokus.edp.validation.shacl.vocabularies.Vocabularies;
import de.fhg.fokus.edp.validation.shacl.ValidationService;
import de.fhg.fokus.edp.validation.shacl.ValidationVerticle;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Launcher;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.dropwizard.MetricsService;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.apache.jena.rdf.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by sim on 16.06.2017.
 */
public class MainVerticle extends AbstractVerticle {

    private Logger log = LoggerFactory.getLogger(getClass());

    private ValidationService validationService;

    @Override
    public void start(Future<Void> startFuture) {
        MetricsService metrics = MetricsService.create(vertx);

        ConfigStoreOptions storeOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray().add("SOME_KEY")));

        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(storeOptions));
        retriever.getConfig(config -> {
            vertx.deployVerticle(ValidationVerticle.class, new DeploymentOptions().setWorker(true).setInstances(8), res -> {
                if (res.succeeded()) {
                    Model v = Vocabularies.vocabularies;
                    validationService = ValidationService.createProxy(vertx, ValidationService.SERVICE_ADDRESS);
                    log.debug("validation verticle deployed and service registered");
                    OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", ar -> {
                        if (ar.succeeded()) {
                            OpenAPI3RouterFactory routerFactory = ar.result();
                            RouterFactoryOptions options = new RouterFactoryOptions().setMountNotImplementedHandler(true);
                            routerFactory.setOptions(options);
                            routerFactory.addHandlerByOperationId("validationReport", this::handleReport);

                            Router router = routerFactory.getRouter();
                            router.route().order(0).handler(CorsHandler.create("*").allowedMethods(Stream.of(HttpMethod.POST).collect(Collectors.toSet())));

                            router.route("/*").handler(StaticHandler.create());

                            HealthCheckHandler hch = HealthCheckHandler.create(vertx);
                            hch.register("buildInfo", future -> vertx.fileSystem().readFile("buildInfo.json", bi -> {
                                if (bi.succeeded()) {
                                    future.complete(Status.OK(bi.result().toJsonObject()));
                                } else {
                                    future.fail(bi.cause());
                                }
                            }));
                            router.get("/health").handler(hch);

                            router.get("/metrics").produces("application/json").handler(routingContext -> {
                                JsonObject m = metrics.getMetricsSnapshot(vertx);
                                if (m != null) {
                                    routingContext.response().setStatusCode(200).end(m.encodePrettily());
                                } else {
                                    routingContext.response().setStatusCode(503).end();
                                }
                            });

                            HttpServer server = vertx.createHttpServer(new HttpServerOptions().setPort(8080));
                            server.requestHandler(router).listen();

                            startFuture.complete();
                        } else {
                            startFuture.fail(ar.cause());
                        }
                    });
                } else {
                    startFuture.fail(res.cause());
                }
            });
        });

    }

    private void handleReport(RoutingContext routingContext) {
        String contentType = routingContext.parsedHeaders().contentType().value();
        String accept = routingContext.getAcceptableContentType();
        validationService.validate(routingContext.getBodyAsString(), contentType, accept, result -> {
            if (result.succeeded()) {
                routingContext.response().end(result.result());
            } else {
                JsonObject error = new JsonObject().put("reason", result.cause().getMessage());
                routingContext.response().setStatusCode(400).end(error.encodePrettily());
            }
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
