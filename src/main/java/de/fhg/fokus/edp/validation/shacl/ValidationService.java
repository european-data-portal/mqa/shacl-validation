package de.fhg.fokus.edp.validation.shacl;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

@ProxyGen
public interface ValidationService {

    String SERVICE_ADDRESS = "service.viaduct.validation";

    static ValidationService create(Handler<AsyncResult<ValidationService>> readyHandler) {
        return new ValidationServiceImpl(readyHandler);
    }

    static ValidationService createProxy(Vertx vertx, String address) {
        return new ValidationServiceVertxEBProxy(vertx, address);
    }

    @Fluent
    ValidationService validate(String content, String contentType, String accept, Handler<AsyncResult<String>> handler);

}
