package de.fhg.fokus.edp.validation.shacl;

import de.fhg.fokus.edp.validation.shacl.vocabularies.Vocabularies;
import io.piveau.utils.JenaUtils;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.topbraid.jenax.util.ARQFactory;
import org.topbraid.shacl.validation.ValidationEngine;
import org.topbraid.shacl.validation.ValidationEngineFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ValidationServiceImpl implements ValidationService {

    private Logger log = LoggerFactory.getLogger(getClass());

    ValidationServiceImpl(Handler<AsyncResult<ValidationService>> readyHandler) {
        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public ValidationService validate(String content, String contentType, String accept, Handler<AsyncResult<String>> handler) {

        RDFNode shape = null;

        Model model = JenaUtils.read(content.getBytes(), contentType);

        if (model.contains(null, RDF.type, DCAT.Catalog)) {
            log.debug("validating against catalog shape");
            shape = DCAT.Catalog;
        } else if (model.contains(null, RDF.type, DCAT.Dataset)) {
            log.debug("validating against dataset shape");
            shape = DCAT.Dataset;
        } else if (model.contains(null, RDF.type, DCAT.Distribution)) {
            log.debug("validating against distribution shape");
            shape = DCAT.Distribution;
        } else if (model.contains(null, RDF.type, DCAT.CatalogRecord)) {
            log.debug("validating against catalogrecord shape");
            shape = DCAT.CatalogRecord;
        } else {
            // fallback
            log.debug("validating against fallback (dataset) shape");
            shape = DCAT.Dataset;
        }

        Dataset dataset = ARQFactory.get().getDataset(model);
        model.add(Vocabularies.vocabularies);

        URI shapesGraphURI = URI.create("urn:x-shacl-shapes-graph:" + UUID.randomUUID().toString());
        ValidationEngine engine = ValidationEngineFactory.get().create(dataset, shapesGraphURI, Vocabularies.shapesGraph, null);

        try {
            engine.applyEntailments();
            ResIterator it = dataset.getDefaultModel().listResourcesWithProperty(RDF.type, shape);

            List<RDFNode> nodes = new ArrayList<>();
            it.forEachRemaining(nodes::add);

            Resource report = engine.validateNodesAgainstShape(nodes, shape.asNode());

            // Resource report = engine.validateAll();
            report.getModel().setNsPrefix("sh", "http://www.w3.org/ns/shacl#");

            if (accept == null || accept.isEmpty()) {
                accept = "text/turtle";
            }
            String output = JenaUtils.write(report.getModel(), accept);

            dataset.removeNamedModel("urn:test");
            if (Vocabularies.vocabularies.containsResource(DCAT.Dataset)) {
                log.warn("debris in vocabularies!");
            }
            handler.handle(Future.succeededFuture(output));
        } catch (InterruptedException e) {
            handler.handle(Future.failedFuture(e));
        }

        return this;
    }

}
