package de.fhg.fokus.edp.validation.shacl;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.serviceproxy.ServiceBinder;

/**
 * Created by sim on 17.06.2017.
 */
public class ValidationVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        ValidationService.create(ready -> {
            if (ready.succeeded()) {
                new ServiceBinder(vertx).setAddress(ValidationService.SERVICE_ADDRESS).register(ValidationService.class, ready.result());
                startFuture.complete();
            } else {
                startFuture.fail(ready.cause());
            }
        });
    }

}
