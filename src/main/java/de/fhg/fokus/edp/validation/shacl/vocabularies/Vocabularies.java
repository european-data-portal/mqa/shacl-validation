package de.fhg.fokus.edp.validation.shacl.vocabularies;

import de.fhg.fokus.edp.validation.shacl.ValidationVerticle;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.topbraid.shacl.arq.SHACLFunctions;
import org.topbraid.shacl.engine.ShapesGraph;
import org.topbraid.shacl.engine.filters.ExcludeMetaShapesFilter;
import org.topbraid.shacl.util.SHACLSystemModel;

public class Vocabularies {

    public static Model vocabularies = ModelFactory.createDefaultModel();
    public static ShapesGraph shapesGraph;

    static {
        Model dcatapShapes = ModelFactory.createDefaultModel();
        RDFDataMgr.read(dcatapShapes, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/dcat-ap.shapes.ttl"), Lang.TURTLE);
        RDFDataMgr.read(dcatapShapes, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/dcat-ap-mandatory-classes.shapes.ttl"), Lang.TURTLE);
        RDFDataMgr.read(dcatapShapes, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/dcat-ap-mdr-vocabularies.shapes.ttl"), Lang.TURTLE);

        Model unionModel = SHACLSystemModel.getSHACLModel();
        MultiUnion unionGraph = new MultiUnion(new Graph[] {
                unionModel.getGraph(),
                dcatapShapes.getGraph()
        });
        dcatapShapes = ModelFactory.createModelForGraph(unionGraph);
        SHACLFunctions.registerFunctions(dcatapShapes);

        shapesGraph = new ShapesGraph(dcatapShapes);
        shapesGraph.setShapeFilter(new ExcludeMetaShapesFilter());

        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/ADMS_SKOS_v1.00.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/continents-skos.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/corporatebodies-skos.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/countries-skos.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/data-theme-skos.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/filetypes-skos.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/frequencies-skos.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/languages-skos.rdf"), Lang.RDFXML);
        RDFDataMgr.read(vocabularies, ValidationVerticle.class.getClassLoader().getResourceAsStream("rdf/places-skos.rdf"), Lang.RDFXML);

    }

}
